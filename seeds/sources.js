var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');
var Promise = require('bluebird');

var range = appRequire('util/range');
var db = appRequire('db');

module.exports = function() {
    return db('datasources').truncate().then(function() {
        return Promise.all(range(1, 10).map(function(n) {
            return db('datasources').insert({
              "name": uuid(),
              "icon": faker.image.imageUrl(),
              "source_meta": {
                "rows": faker.random.number(),
                "columns": faker.random.number()
              }
            });
        }));
    });
};
