require('./appRequire')();
// So that JSON.stringify(date) doesn't fuck things up
Date.prototype.toJSON = Date.prototype.toString;

var config = appRequire('config');

// setup the server engine
appRequire('engine').setup(function(teteris) {
  // load up teteris workers
  teteris.setup()
    .then(() => {
      console.log("workers and middlewares mounted");
    });
});
