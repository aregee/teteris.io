var orm = appRequire('orm');

var db = orm;
var _ = require('lodash');
module.exports = function(teteris) {

  teteris('dbcache').on('error', (e) => {
    console.log('WORKER ERROR', e);
  });

  teteris('dbcache').on('request', (inp, rep) => {
    console.log("Received worker: ", inp);
    console.log(inp);
    var inp = JSON.parse(inp);
    var args = _.isObject(inp.args) ? inp.args : {};
    if (args.hasOwnProperty('data') && args.hasOwnProperty('time')) {
      db.cache.set(inp.fname, inp.args.data, inp.args.time)
        .then((response) => {
          rep.end(response);
        })
        .catch((err) => {
          var errObj = {};
          errObj.message = "db Error";
          errObj.error = err;
          rep.end(errObj);
        });
    } else {
      console.log(inp.fname);
      db.cache.get(inp.fname)
        .then((val) => {
          console.log(val);
          console.log("herere<<<<")
          rep.end(val);
        }).catch((err) => {
          var errObj = {};
          errObj.message = "db Error";
          errObj.error = err;
          rep.end(errObj);
        });
    }
  });
};
