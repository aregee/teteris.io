var orm = appRequire('orm');

var db = orm.table;


module.exports = function (teteris) {

  teteris('dbInsertResource').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('dbInsertResource').on('request', function(inp, rep) {
    console.log("Received worker: ", inp);
    console.log(inp.fname);
    db(`${inp.fname}`).insert(inp.args.data)
      .then(function (data) {
        rep.end(data);
      })
      .catch(function (err) {
        rep.end(err);
      });
    });
};
