exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('product_category', function(t) {
            t.increments('id');
            t.string('name', 400);
            t.string('type', 400);
            t.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('product_category')
    ]);
};
