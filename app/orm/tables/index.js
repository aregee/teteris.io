module.exports = function(orm) {

  require('./users')(orm);
  //('./countries')(orm);
  require('./cities')(orm);
  require('./goal')(orm);
  require('./goals')(orm);
  require('./goal_category')(orm);
  require('./asset')(orm);
  require('./assets')(orm);
  require('./asset_category')(orm);

};
