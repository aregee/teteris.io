exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('products', function(t) {
            t.increments('id');
            t.string('name', 400);
            t.integer('user_id').index();
            t.integer('product_category_id');
            t.json('meta', true);
            t.double('value');
            t.string('type', 500);
            t.timestamps();
        }),
        knex.schema.createTable('product', function (t) {
          t.increments('id');
          t.integer('user_id');
          t.integer('product_id');
          t.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('products'),
        knex.schema.dropTable('product')
    ]);
};
