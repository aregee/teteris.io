"use strict";
var path = require('path');
//import Tabel from 'tabel';

let repath = (p) => {
  return path.normalize(__dirname + '/../..' + p);
};

let appPackage = repath('/package.json');

const properties = {
  path: path,

  name: appPackage.name,

  api: {
    defaultVersion: 'v1',
    versions: ['v1']
  },
  serviceApi: {
    development: {
      baseUrl: 'http://localhost:3030'
    },
    production: {
      baseUrl: 'http://localhost:3000'
    }
  },
  server: {
    port: 55555,
    host: 'tcp://localhost:',
    uploadsDir: repath('/storage/uploads'),
    publicDir: repath('/public')
  },

  redis: {
    development: {
      host: '0.0.0.0',
      port: '6379',
      dbCachePrefix: '_div.cache'
    },
    production: {
      host: 'localhost',
      port: '6379',
      dbCachePrefix: '_div.cache'
    },
    staging: {
      host: 'localhost',
      port: '6379',
      dbCachePrefix: '_div.cache'
    }
  },
  orm: {
    cacheEnabled: true,

    development: {
      db: {
        client: 'postgresql',
        connection: {
          database: 'zam_dev',
          host: "localhost",
          port: 5432,
          user: 'dev',
          password: 'dev'
        },
        pool: {
          min: 2,
          max: 10
        },
        migrations: 'knex_migrations'
      },
      // redis config is optional, is used for caching by tabel
      redis: {
        host: '0.0.0.0',
        port: '6379',
        keyPrefix: 'dev.api.'
      }
    },
    production: {
      db: {
        client: 'postgresql',
        connection: {
          database: 'zam_dev',
          host: "zam-staging.cukpuueefpwt.ap-southeast-1.rds.amazonaws.com",
          port: 5432,
          user: 'zam_dev',
          password: 'zamdevaws'
        },
        pool: {
          min: 2,
          max: 10
        },
        migrations: 'knex_migrations'
      },
      // redis config is optional, is used for caching by tabel
      redis: {
        host: '0.0.0.0',
        port: '6379',
        keyPrefix: 'dev.api.'
      }
    }
  },
  db: {
    development: {
      client: 'postgresql',
      connection: {
        database: 'wk_demo',
        host: "localhost",
        port: 5432,
        user: 'dev',
        password: 'dev'
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: 'knex_migrations'
    },

    staging: {
      client: 'postgresql',
      connection: {
        database: 'wk_demo',
        host: "localhost",
        port: 5432,
        user: 'dev',
        password: 'dev'
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: 'knex_migrations'
    },

    production: {
      client: 'postgresql',
      connection: {
        database: 'wk_demo',
        host: "localhost",
        port: 5432,
        user: 'dev',
        password: 'dev'
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: 'knex_migrations'
    }
  }
};

module.exports = properties;
