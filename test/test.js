var Promise = require('bluebird');
var _ = require('lodash');
var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');

module.exports = function(teteris) {
  return Promise.all(_.range(0, 1).map(function(n) {
    var password = "notebook";
    return teteris('dbInsertResource', {
      fname: "users",
      args: {
        data: {
          username: "rahul.nbg@gmail.com",
          email: "rahul.nbg@gmail.com",
          full_name: faker.name.firstName() + ' ' + faker.name.lastName(),
          password: bcrypt.hashSync(password),
          plain_password: password,
          social_accounts: {
            "facebook_id": uuid(),
            "twitter_id": uuid(),
            "googleplus_id": uuid()
          },
          user_location: {
            "lat": 33.33,
            "lng": 77.61
          },
          details: {
            age: faker.helpers.randomNumber(19, 60),
            gender: n % 2 === 0 ? 'M' : 'F',
          },
          imported_data: faker.helpers.contextualCard()
        }
      }
    });
  }));
};
