var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');
var Promise = require('bluebird');
var _ = require('lodash');
var range = appRequire('util/range');
var db = appRequire('db');
var transformations = [ 'Select',
'TableSelect',
'Filter',
'Join',
'Sort',
'Sample',
'Union',
'Standardize',
'Dedupe',
'Regexparser'];

module.exports = function() {
    return db('operations').truncate().then(function() {
        return Promise.all(transformations.map(function(n) {
            return db('operations').insert({
              "name": n.toLowerCase(),
              "icon": faker.image.imageUrl(),
              "operations_meta": {
                ui_component: n.toLowerCase()
              }
            });
        }));
    });
};
