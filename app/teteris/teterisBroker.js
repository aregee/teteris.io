"use strict";
var _ = require('lodash');

let teterisBroker = (core, opts) => {

  let options = _.isUndefined(opts) ? {} : opts;
  options.port = opts.port ? opts.port : "55555";
  var uri = ["tcp://*:", options.port].join('');
  var broker = new core.Broker(uri);
  return broker;

};

module.exports = teterisBroker;
