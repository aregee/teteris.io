exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('assets', function(t) {
            t.increments('id');
            t.string('name', 400);
            t.integer('user_id').index();
            t.integer('asset_category_id');
            t.json('meta', true);
            t.float('value');
            t.string('type', 500);
            t.timestamps();
        }),
        knex.schema.createTable('asset', function (t) {
          t.increments('id');
          t.integer('user_id');
          t.integer('asset_id');
          t.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('assets'),
        knex.schema.dropTable('asset')
    ]);
};
