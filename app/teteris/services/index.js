module.exports = function (teteris) {
    // load up all table definitions here
    require('./dbstats')(teteris);
    require('./dbtables')(teteris);
    require('./dbcache')(teteris);
    require('./dbinsert')(teteris);
    require('./dbtableitem')(teteris);
    require('./dbsocialauth')(teteris);
};
