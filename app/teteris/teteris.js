"use strict";
var Promise = require('bluebird');
var _ = require('lodash');

var config = appRequire('config');
var Service = require('./teterisWorker');
var Broker = require('./teterisBroker');
var CoreService = require('./CoreService');

module.exports = teteris;


function teteris(service, args) {
  var worker = teteris.services[service];
  return worker;
};

teteris.loadServices = function() {
  throw 'override loadServices, and load services in it';
};

teteris.core = require('pigato');


teteris.listen = function(opts) {
  teteris.broker = Broker(teteris.core, opts);
  return teteris.broker.start();
};

teteris.coreServices = function(opts) {
  teteris.coreServices = CoreService(teteris.core, opts);
  return teteris.coreServices.start();
};

teteris.setup = function() {
  teteris.services = {};

  teteris.Service = function(opts) {
    teteris.services[opts.serviceName] = Service(teteris, opts);
    return teteris;
  };
  teteris.loadServices(teteris);

  // return a promise which ensures that all registered workers for services are started
  return Promise.all(_.map(teteris.services, function(service) {
    return service.start();
  }));
};
