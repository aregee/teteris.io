var Promise = require('bluebird');
var _ = require('lodash');
var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');

module.exports = function(teteris) {
  return Promise.all(_.range(1, 8).map(function(n) {
    return teteris('dbInsertResource', {
      fname: "products",
      args: {
        data: {
          user_id: 10,
          product_category_id: n,
          name: `${faker.name.firstName()}'s random asset`,
          value: n > 6 ? faker.helpers.randomNumber(190000, 6000000) : faker.helpers.randomNumber(190000, 6000000),
          meta: faker.helpers.createTransaction(),
          type: n > 6 ? 'liability' : 'asset',
        }
      }
    });
  }));
};
