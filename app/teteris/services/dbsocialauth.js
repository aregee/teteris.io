"use strict";
var config = appRequire('config');
let server = config('server');

module.exports = function(teteris) {
  teteris.Service({
    serviceName: 'socialAuth',
    port: server.port,
    host: server.host
  });
};
