exports.up = function (knex, Promise) {

  return Promise.all([
    knex.schema.createTable('user_goals', function(t) {
      t.increments('id');
      t.integer('goal_id').index();
      t.integer('goal_category_id');
      t.json('meta', true);
      t.string('name', 150);
      t.float('ammount');
      t.timestamps();
    })
  ]);
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('user_goals')
  ]);
}
