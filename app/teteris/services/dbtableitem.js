"use strict";
var config = appRequire('config');

let server = config('server');

module.exports = function (teteris) {
    teteris.Service({
        serviceName       : 'dbTableItem',
        port              : server.port,
        host              : server.host
    });
};
