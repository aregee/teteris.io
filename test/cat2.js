var Promise = require('bluebird');
var _ = require('lodash');
var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');


module.exports = function(teteris) {
  return Promise.all(['credit card', 'loan'].map(function(n) {
    return teteris('dbInsertResource', {
      fname: "product_category",
      args: {
        data: {
          name: n,
          type: 'liability',
        }
      }
    });
  }));
};
