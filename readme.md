

## Teteris.io

**Setup**

```
  after cloning the repo, cd into the repo and do following
  run chown -R yourusername /path/to/global/node_modules/folder
  run ./setup.sh from repository root and let it finish
  run ./bin/dev_start.sh to start the local server
```

Setup Postgres, check `app/config/properties.js` for more details.

**Register A Service**

Add a new file in `teteris/services` with Service name and do proper module export from
`teteris/services` in index.js.

```
  module.exports = function (teteris) {
      teteris.Service({
          serviceName       : 'dbStatus',
          port              : '55555'
      });
  };
```

After registering the serivce configuration, we have to add service handler in
`workers` directory in the following manner:

```
// workers/dbstats.js
var Promise = require('bluebird');
var _ = require('lodash');

var config = appRequire('config');
var db = appRequire('db');
module.exports = function (teteris) {

  teteris('dbStatus').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('dbStatus').on('request', function(inp, rep) {
    console.log("Received worker: ", inp);
    // using worker to set a value in db cache for 20 secs and chain
    // the request further with a delay of 21 secs
    // if it works core db service is up and running
    db.cache('foo', {
      x: 1,
      y: 2
    }, 20).then(function() {
      return db.cache('foo').then(function(val) {
        console.log(_.isObject(val));
        return _.isObject(val);
      });
    }).delay(21).then(function() {
      return db.cache('foo').then(function(val) {
        console.log(_.isObject(val));
        if (val === null) {
          rep.end({status: true});
        } else {
          rep.end({status: false});
        }
      });
    });
  });
};
```

**Express Example**

```
  var app = module.exports = require('express')();
  var teteris = appRequire('teteris');

  app.get('/sources', function(req, res) {
    teteris('dbTableList', {
      fname: 'datasources',
      args: {
        orderBy: 'id',
        query: {
          page: 1
        }
      }
    }).then(function (data) {
      res.send({datasources: data});
    }).catch(function (err) {
      res.send({msg: err});

    });
  });
```


**Test**

execute following from project root:
* `node sdk/teteris/main.js
