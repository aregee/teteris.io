var teteris = appRequire('teteris').teteris;
var log = require('metalogger')();
var cluster = require('cluster');
var config = require('../config');


var server = config('server');

teteris.loadServices = function () {
  appRequire('teteris/services')(teteris);
  appRequire('workers')(teteris);
};

function configureLogging() {
  if ('log' in config) {

    if ('plugin' in config.log) {
      process.env.NODE_LOGGER_PLUGIN = config.log.plugin;
    }
    if ('level' in config.log) {
      process.env.NODE_LOGGER_LEVEL = config.log.level;
    }

    if ('customlevels' in config.log) {
      for (var key in config.log.customlevels) {
        process.env['NODE_LOGGER_LEVEL_' + key] = config.log.customlevels[key];
      }
    }
  }
};

exports.setup = function(callback) {
  configureLogging();
  //teteris.start(function () {});
  var isClusterMaster = (cluster.isMaster && (process.env.NODE_CLUSTERED === '1'));
  var isHttpThread = true;

  if (isClusterMaster) {
    isHttpThread = false;
  }

  log.debug('is http thread ? ' + isHttpThread);


  if (isHttpThread) {
    teteris.listen({
      port: server.port
    });
    teteris.coreServices({port: server.port, broker: teteris.broker});
  }

  // If we are not running a cluster at all:
  if (isClusterMaster) {
    require('./clustering').setup();
  }
  if (!isClusterMaster && cluster.isMaster) {
    log.notice("Teteris server instance listening on port " + config.server.port);
  }

  /** setup some basic middlewares can be plugged here in Teteris **/

  if (process.env.NODE_ENV === 'development') {
    //app.use(require('errorhandler')({log: true}));
    // Do something about logging stuff
  }

  if (process.env.NODE_ENV === 'production') {
    // trust proxy in production from local nginx front server
    // app.set('trust proxy', 'loopback')
    // setup callback to trust shit on loopback only
    // until we find secure way to expose zmq on network
    // with some auth layer
  };

  callback(teteris);
};
