module.exports = function (teteris) {
    // load up all table definitions here
    require('./dbstats')(teteris);
    require('./dbcache')(teteris);
    require('./dbtables')(teteris);
    require('./dbinzert')(teteris);
    require('./dbtableitem')(teteris);
    require('./teterisSociallogin')(teteris);
};
