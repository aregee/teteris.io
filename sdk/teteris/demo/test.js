var Promise = require('bluebird');
var _ = require('lodash');

module.exports = function(teteris) {
  var promises = _.map(_.range(0, 2), function(i) {
    return Promise.props({
        status: teteris('dbStatus', {
          fname: 'foo',
          args: [3,4,5,6,]
        }),
        cache: teteris('dbcache', {
          fname: 'bar',
          args: [3,4,5,6,]
        }),
        users: teteris('dbTableList', {
          fname: 'users',
          args: {
            orderBy: 'id',
            query: {
              page: i
            }
          }
        }),
        countries: teteris('dbTableList', {
          fname: 'countries',
          args: {
            orderBy: 'id',
            query: {
              page: i
            }
          }
        }),
        citites: teteris('dbTableList', {
          fname: 'cities',
          args: {
            orderBy: 'id',
            query: {
              page: i
            }
          }
        }),
        echo: teteris('$dir', 'dbTableList')
      });
  });
  Promise.all(promises)
  .then((data) => {
    console.log(data);
  })
  .catch((err) => {
    console.log(err);
  });
  // setInterval(function () {
  // }, 150);
};
