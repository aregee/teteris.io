var Promise = require('bluebird');
var _ = require('lodash');
var Client = require('./teterisClient');

module.exports = teteris;

function teteris(service, args) {
  var handle = teteris.handle(service, args);
  return handle;
};

teteris.core = require('pigato');

teteris.bindClient = function(opts) {
  teteris.client = new Client(teteris.core, opts);
  teteris.handle = function(method, options) {

    // instantiate options as an empty object literal
    options = _.isUndefined(options) ? {} : options;

    return new Promise(function(resolve, reject) {
      teteris.client.request(method, options, undefined,
        function(err, data) {
          if (err) {
            reject(err);
          }
          resolve(data);
        }, {
          timeout: 1000
        }
      );
    });

  };
  teteris.client.start();
  return teteris;
};
