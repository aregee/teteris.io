var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');
var Promise = require('bluebird');

var range = appRequire('util/range');
var db = appRequire('db');

module.exports = function() {
    return Promise.props({
      'operations': db('operations').all(),
      'pipeline_operations': db('pipeline_operations').truncate()
    }).then(function (data) {
      return Promise.all(data.operations.map(function (n) {
        var predictedPorts = {};
        if (n.name === 'Join') {
          predictedPorts =  {
            "inputs": [{"port": faker.lorem.words()[0], required: true}, {"port": faker.lorem.words()[2], required: true}],
            "outputs": [{"port": faker.lorem.words()[0], "required": true}, {"port": faker.lorem.words()[0], "required": true}, {"port": faker.lorem.words()[0], "required": true}]
          };
        } else {
          predictedPorts =  {
            "inputs": [{"port": faker.lorem.words()[0], required: true}],
            "outputs": [{"port": faker.lorem.words()[0], "required": true}]
          };
        }
        return db('pipeline_operations').insert({
                  "operation_id": n.id,
                  "ports": predictedPorts,
                  "input_ports": {},
                  "output_ports": {},
                  "status": {},
                  "node_cordinates": {},
                  "type": "operation"
              });
      }));
    });
};
