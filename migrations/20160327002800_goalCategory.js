exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('goal_category', function(t) {
            t.increments('id');
            t.string('name', 400);
            t.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('goal_category')
    ]);
};
