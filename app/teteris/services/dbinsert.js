"use strict";
var config = appRequire('config');
let server = config('server');

module.exports = function(teteris) {
  teteris.Service({
    serviceName: 'dbInsertResource',
    port: server.port,
    host: server.host
  });
};
