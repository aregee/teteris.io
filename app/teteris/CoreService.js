"use strict";
var _ = require('lodash');

let CoreService = (core, opts) => {

  let options = _.isUndefined(opts) ? {} : opts;
  options.port = opts.port ? opts.port : "55555";
  let uri = ["tcp://*:", options.port].join('');
  let csrv = new core.services.Directory(uri, {
    intch: opts.broker.conf.intch // internal pub/sub channel
  });
  return csrv;

};

module.exports = CoreService;
