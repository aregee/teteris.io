var uuid = require('uuid');
var _ = require('lodash');
var Promise = require('bluebird');

module.exports = Client;

function Client(core, opts) {
  var uri = ['tcp://', opts.host, ':', opts.port].join('');
  return new core.Client(uri);
};
