var orm = appRequire('orm');

var db = orm.table;


module.exports = function (teteris) {

  teteris('dbTableList').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('dbTableList').on('request', function(inp, rep) {
    console.log("Received worker: ", inp);
    console.log(inp.fname);
    db(inp.fname).forPage(inp.args.query.page).orderBy(inp.args.orderBy).all()
      .then(function (data) {
        rep.end(data);
      })
      .catch(function (err) {
        rep.end(err);
      });
    });
};
