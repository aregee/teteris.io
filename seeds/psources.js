var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');
var Promise = require('bluebird');

var range = appRequire('util/range');
var db = appRequire('db');

module.exports = function() {
    return Promise.props({
      'datasources': db('datasources').all(),
      'pipeline_datasources': db('pipeline_datasources').truncate()
    }).then(function (data) {
      return Promise.all(data.datasources.map(function (n) {
        return db('pipeline_datasources').insert({
                  "datasource_id": n.id,
                  "ports": {
                    "inputs": [],
                    "outputs": [{"port": 1, "required": true}]
                  },
                  "input_ports": {},
                  "output_ports":{},
                  "status": {},
                  "node_cordinates": {},
                  "type": "datasource"
              });
      }));
    });
};
