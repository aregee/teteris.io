var orm = require('../orm');

var db = orm;

var _ = require('lodash');


module.exports = function (teteris) {

  teteris('dbStatus').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('dbStatus').on('request', function(inp, rep) {
    console.log("Received worker: ", inp);
    // using worker to set a value in db cache for 20 secs and chain
    // the request further with a delay of 21 secs
    // if it works core db service is up and running
    db.cache.set('foo', {
      x: 1,
      y: 2
    }, 20).then(function(data) {
      console.log(data);
      return db.cache.get('foo').then(function(val) {
        console.log(_.isObject(val));
        return _.isObject(val);
      });
    }).delay(21).then(function() {
      return db.cache.get('foo').then(function(val) {
        console.log(_.isObject(val));
        if (val === null) {
          rep.end({status: true});
        } else {
          rep.end({status: false});
        }
      });
    });
  });
};
