module.exports = function(orm) {
  orm.defineTable({
    name: 'product_category',
    props: {
      key: 'id',
      // default key column, can be ['user_id', 'post_id'] for composite keys

      autoId: false,
      // set this to true if you want the orm to generate a 36 character
      // uuid for your inserts. The orm checks for uniqueness of the uuid
      // when its generating it.
      // Can generate composite keys.
      // If using autoId on postgres, use uuid column for your key(s)
      // If using autoId on any other db, use a 36 length varchar

      perPage: 25,
      // standard batch size per page used by `forPage` method
      // table.forPage(page, perPage) method uses offset
      // avoid that and use a keyset in prod (http://use-the-index-luke.com/no-offset)

      timestamps: true
        // set to `true` if you want auto timestamps or
        // timestamps: ['created_at', 'updated_at'] (these are defaults when `true`)
        // will be assigned in this order only
    },

    // used to process model and collection results fetched from the db
    // override as you need to
    processors: {
      model(row) {
          return row;
        },
        collection(rows) {
          return rows;
        }
    },

    relations: {
      product: () => {
        return this.hasMany('products', 'product_category_id')
      }
    },

    joints: {
      // add joints here
    },

    scopes: {
      // add scopes here
    },

    methods: {
      // add methods here
    }
  });
};
