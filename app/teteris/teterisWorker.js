"use strict";
var uuid   = require('uuid');
var _      = require('lodash');

module.exports = Service;

function Service (teteris, opts) {

    var uri = [opts.host, opts.port].join('');
    var service = new teteris.core.Worker(uri, opts.serviceName);
    return service;
};
