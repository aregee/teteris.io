var orm = appRequire('orm');
var _ = require('lodash');
var db = orm.table;


module.exports = function(teteris) {

  teteris('socialAuth').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('socialAuth').on('request', function(inp, rep) {
    console.log("Received worker: ", inp);
    console.log(inp.fname);
    var socialtype = inp.args.data.social.type;
    var socialprofile = inp.args.data.social.profile;
    var token = inp.args.token;
    db("users").find("email", socialprofile.email)
      .then(function(user) {
        if (user) {
          var data = {};
          data = {
            social_accounts: {},
            imported_data: {},
            full_name: socialprofile.first_name,
            email: socialprofile.email,
            username: socialprofile.email
          };

          var social = {};
          social[socialtype] = socialprofile.id;
          var socialImported = {};
          socialImported[socialtype] = socialprofile;
          _.extend(data, {
            social_accounts: social
          }, {
            imported_data: socialImported
          });
          _.merge(user, data);
          var userData = user;
          return db("users").update(user.id, userData);
        } else {
          return db("users").createBySocialId({
            social_type: socialtype,
            social_id: socialprofile.id,
            profile: socialprofile
          });
        }
      }).then(function(user) {
        console.log(user);
        console.log(">>>ASdasd>")
        orm.cache.set(token, user.id, 2000000)
          .then(function(contents) {
            user.access_token = token;
            console.log(user);
            rep.end(user);
          });
      }).catch((err) => {
        throw err;
      });
  });
};
