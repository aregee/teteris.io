exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('goals', function(t) {
            t.increments('id');
            t.string('name', 400);
            t.integer('user_id').index();
            t.integer('goal_category_id');
            t.json('meta', true);
            t.float('ammount');
            t.string('duration');
            t.timestamps();
        }),
        knex.schema.createTable('goal', function (t) {
          t.increments('id');
          t.integer('user_id');
          t.integer('goal_id');
          t.timestamps();
        })
    ]);
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('goals'),
        knex.schema.dropTable('goal')
    ]);
};
