var orm = appRequire('orm');
var _ = require('lodash');
var db = orm.table;


module.exports = function(teteris) {
  //
  // teteris('dbTableItem').on('error', function(e) {
  //   console.log('WORKER ERROR', e);
  // });
  //
  // teteris('dbTableItem').on('request', function(inp, rep) {
  //   console.log("Received worker: ");
  //   var inp = JSON.parse(inp);
  //   console.log(inp.args);
  //   var relations = _.isArray(inp.args.eagarLoad) ? inp.args.eagarLoad : [];
  //   db(inp.fname).eagerLoad(...relations)
  //     .find('id', inp.args.id)
  //     .then(function(data) {
  //       console.log(data);
  //       rep.end(data);
  //     })
  //     .catch(function(err) {
  //       console.log(err);
  //       rep.end(err);
  //     });
  // });

  teteris('dbTableItem').on('error', function(e) {
    console.log('WORKER ERROR', e);
  });

  teteris('dbTableItem').on('request', function(inp, rep) {
    console.log("Received worker: ");
    console.log(inp.args);
    var inp = JSON.parse(inp);
    var relations = _.isArray(inp.args.eagarLoad) ? inp.args.eagarLoad : [];
    db(inp.fname)
      .eagerLoad(...relations)
      .find(inp.args.queryBy, inp.args.payload)
      .then(function(data) {
        rep.end(data);
      })
      .catch(function(err) {
        console.log(err);
        rep.end(err);
      });
  });
};
