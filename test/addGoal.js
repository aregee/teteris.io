var Promise = require('bluebird');
var _ = require('lodash');
var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');

module.exports = function(teteris) {
  return Promise.all(_.range(1, 10).map(function(n) {
    var password = faker.internet.password();
    return teteris('dbInsertResource', {
      fname: "goals",
      args: {
        data: {
          user_id: 1,
          goal_category_id: n,
          name: `${faker.name.firstName()}'s random wish`,
          ammount: faker.helpers.randomNumber(190000, 6000000),
          meta: faker.helpers.contextualCard(),
          duration: faker.helpers.randomNumber(1, 10)
        }
      }
    });
  }));
};


module.exports = function(teteris) {
  return Promise.all(['Childrens Education', 'House Downpayment', 'Vacation', 'Retirement'].map(function(n) {
    return teteris('dbInsertResource', {
      fname: "goal_category",
      args: {
        dta: {
          name: n,
        }
      }
    });
  }));
};
