var Promise = require('bluebird');
var _ = require('lodash');
var uuid = require('uuid');
var faker = require('faker');
var bcrypt = require('bcryptjs');


module.exports = function(teteris) {
  return Promise.all(['gold', 'property', 'provident fund', 'equity fund', 'direct equity', 'bank account'].map(function(n) {
    return teteris('dbInsertResource', {
      fname: "product_category",
      args: {
        data: {
          name: n,
          type: 'asset',
        }
      }
    });
  }));
};
