var bluebird = require('bluebird');
var lodash = require('lodash');
var config = appRequire('config');
var tables = require('./tables');
var Tabel = require('tabel');

const Promise = bluebird;
const _ = new lodash;
var orm = new Tabel(config('orm'));

require('./tables')(orm);


module.exports = orm.exports;
